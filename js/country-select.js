/* global bioH */

/**
 * @param bioH.ajax
 * @param bioH.isPageProduct
 * @param bioH.countryCode
 * @param bioH.countryName
 */

jQuery( document ).ready( function( $ ) {
	let crtSelect = $( '.ctr-select' );
	let deliveryResultText = $( '.delivery-result-text' );
	let isPageProduct = bioH.isPageProduct;

	jQuery( crtSelect ).countrySelect( {
		onlyCountries: countryList,
		responsiveDropdown: true,
	} );

	let cUserCountryCode = bioH.countryCode.toLowerCase();
	$( '.country-select .selected-flag .flag' ).removeClass( countryList[ 0 ] );
	crtSelect.val( 'Choose your location' );

	let country = crtSelect;

	if ( countryList.includes( cUserCountryCode ) ) {
		crtSelect.countrySelect( 'selectCountry', cUserCountryCode );
		deliveryResultText.html( 'A few seconds...' ).show();
		$( '.delivery-error' ).hide();
		populate_fields();
		$( '.delivery-time-text' ).html( '<div class=\'flag\' style=\'background-position: ' + $( '.country-select .flag' ).css( 'background-position' ) + '\'></div> <p>' + bioH.countryName + '</p>' );
	} else if ( isPageProduct ) {
		$( '.delivery-info-txt' ).html( 'Delivery time not available, contact us for more information.' );
	}

	crtSelect.change( function( e ) {
		if ( e.target.value === '' ) {
			deliveryResultText.html( '' );
			deliveryResultText.hide();
			$( '.delivery-error' ).show();
		} else {
			deliveryResultText.html( 'A few seconds...' ).show();
			$( '.delivery-error' ).hide();
		}
		populate_fields();
		$( '.delivery-time-text' ).html( '<div class=\'flag\' style=\'background-position: ' + $( '.country-select .flag' ).css( 'background-position' ) + '\'></div> <p>' + country.val() + '</p>' );
	} );

	crtSelect.on( 'click', function() {
		$( '.selected-flag' ).trigger( 'click' );
	} );

	function populate_fields() {
		let data = {
			'action': 'cf7_populate_values',
			'country': country.val(),
		};
		let all_values = '';

		$.post( bioH.ajax, data, function( response ) {
				all_values = response;
				if ( all_values.delivery_time ) {
					let d_min = all_values.delivery_time.min;
					let d_max = all_values.delivery_time.max;
					$( '.delivery-result-text' ).html( '<table class="w-100 has-background"><tbody><tr><th style="padding-left: 0">Standard Delivery:</th><td>' + d_min + '-' + d_max + '  working days</td></tr></tbody></table><p class="text-muted text-italic">*Subject to placing your order before specific cut-off times. All shipping times are approximations and cannot be guaranteed. Note that these may increase during holidays.</p> <hr><p class="text-muted p-15"></p>' );
					if ( isPageProduct )
						$( '.delivery-info-txt' ).html( '<strong>Approximately ' + d_min + '-' + d_max + ' days to reach your destination</strong>' );
					$( '.delivery-time-country-name' ).html( country.val() );

				}
			},
			'json'
		);
	}
} );