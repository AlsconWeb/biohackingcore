jQuery( document ).ready( function( $ ) {
	$( document ).on( 'click', '.btn-filter', function( e ) {
		var _this = $( this );
		var slug = '';
		var is_selected = false;
		var loading_html = '<div class="loading-overlay"><img class="loading-img" src="/wp-content/themes/storefront-child/images/loading.gif" width=50/></div>';

		is_selected = toggleCategoryButton( _this );
		if ( is_selected ) {
			slug = _this.data( 'slug' );
		}

		$( '.products-wrapper' ).append( loading_html );

	} );

	function toggleCategoryButton( element ) {
		var selected = element.hasClass( 'selected' );
		$( '.btn-filter' ).removeClass( 'selected' );
		if ( ! selected ) {
			element.addClass( 'selected' );
			return true;
		}
		return false;
	}

	$( document ).on( 'mouseenter', '.sort-by-type .ubermenu-submenu .ubermenu-tab.ubermenu-item a', function() {
		var _this = $( this );
		setTimeout( function() {
			if ( ! _this.parent().hasClass( 'ubermenu-active' ) ) {
				_this.parent().addClass( 'ubermenu-active' );
			}
			_this.parent().siblings().removeClass( 'ubermenu-active' );
		}, 200 );
	} );

	$( document ).mouseup( function( e ) {
		var container = $( '.delivery-time-container' );

		// if the target of the click isn't the container nor a descendant of the container
		if ( ! container.is( e.target ) && container.has( e.target ).length === 0 ) {
			setTimeout( function() {
				container.removeClass( 'open' );
			}, 100 );
		}
	} );

	$( document ).on( 'click', '.delivery-time', function( e ) {
		if ( $( '.delivery-time-container' ).hasClass( 'open' ) ) {
			setTimeout( function() {
				$( '.delivery-time-container' ).removeClass( 'open' );
			}, 100 );
		} else {
			$( '.delivery-time-container' ).addClass( 'open' );

		}
	} );

	$( document ).on( 'click', '.filter-wrapper .tab .tablinks', function( e ) {
		var _this = $( this );
		var tab_num = _this.data( 'tab' );
		setTimeout( function() {
			$( '.filter-wrapper .tab .tablinks' ).removeClass( 'active' );
			_this.addClass( 'active' );
			$( '.filter-wrapper .tabcontent' ).removeClass( 'open' );
			$( '.filter-wrapper .tabcontent[data-tab="' + tab_num + '"]' ).addClass( 'open' );
		}, 500 );
	} );

	$( document ).tooltip( {
		show: null,
		items: '.d-tooptip',
		open: function( event, ui ) {
			ui.tooltip.animate( { top: ui.tooltip.position().top - 10 }, 'fast' );
		}
	} );

	$( document ).ready( function() {
		$( '.fl-node-61275ae03ab6d, .fl-node-61275ae03ab71' ).wrapAll( '<div class="mouseleave-effect" />' );

		var badgeStr = '<span class="badge">30% off</span>';
		$( '.main-navigation ul.nav-menu > li:first-child > a' ).append( badgeStr );

		$( 'body' ).removeClass( 'right-sidebar' );
		$( 'body .hoverable' ).each( function( idx ) {
			$( this ).find( 'svg:first-child' ).mouseover( function( e ) {
				var _idx = idx + 1;
				var igl_bg = $( `.ilg-bgs img[data-idx="${_idx}"]` );
				if ( ! igl_bg.hasClass( 'active' ) ) {
					$( '.ilg-bgs img.active' ).hide();
					$( '.ilg-bgs img' ).removeClass( 'active' );
					igl_bg.addClass( 'active' );
					igl_bg.fadeIn( 500 );
				}
			} );

			$( this ).find( 'svg:first-child' ).on( 'mouseleave', function() {
				$( '.ilg-bgs img.active' ).hide();
				$( '.ilg-bgs img' ).removeClass( 'active' );
				$( '.ilg-bgs img:first-child' ).addClass( 'active' );
				$( '.ilg-bgs img:first-child' ).fadeIn( 500 );
			} );
		} );

	} );

	$( '.mini-cart' ).click( function( e ) {
		e.preventDefault();
		$( '.woo_amc_container_wrap.woo_amc_container_wrap_right' ).addClass( 'woo_amc_show' );
	} );

	$( document ).mouseup( function( e ) {
		let folder = $( '.woo_amc_show' );

		if ( ! folder.is( e.target ) && folder.has( e.target ).length === 0 ) {
			$( '.woo_amc_container_wrap.woo_amc_container_wrap_right' ).removeClass( 'woo_amc_show' );
		}
	} );


	if ( $( '#particles-js' ).length ) {

		// Include images
		let img_src = [
			'https://biohackingcore.com/wp-content/uploads/2022/07/type-1.png',
			'https://biohackingcore.com/wp-content/uploads/2022/07/type-2.png',
			'https://biohackingcore.com/wp-content/uploads/2022/07/type-3.png'
		];

		// Name images included
		let image_type = img_src.map( function( cuurentEl, index ) {
			return 'image' + index
		} );

		particlesJS( 'particles-js',
			{
				'particles': {
					'number': {
						'value': 30, // No of images
						'density': {
							'enable': true,
							'value_area': 500 // Specify area (Lesser is greater density)
						}
					},
					'color': {
						'value': '#5affd4'
					},
					'shape': {
						'type': image_type, // Add images to particle-js
						'stroke': {
							'width': 0,
						},
						'polygon': {
							'nb_sides': 4
						}
					},
					'opacity': {
						'value': 0.4, // Adjust opactiy
						'random': false,
						'anim': {
							'enable': false,
							'speed': 1,
							'opacity_min': 0.1,
							'sync': false
						}
					},
					'size': {
						'value': 10, // Adjust the image size
						'random': false,
						'anim': {
							'enable': false,
							'speed': 10,
							'size_min': 40,
							'sync': false
						}
					},
					'line_linked': {
						'enable': false,
						'distance': 200,
						'color': '#ffffff',
						'opacity': 1,
						'width': 2
					},
					'move': {
						'enable': true,
						'speed': 1,   // Speed of particle motion
						'direction': 'none',
						'random': false,
						'straight': false,
						'out_mode': 'out',
						'bounce': false,
						'attract': {
							'enable': false,
							'rotateX': 600,
							'rotateY': 1200
						}
					}
				},
				'interactivity': {
					'detect_on': 'canvas',
					'events': {
						'onhover': {
							'enable': false,
							'mode': 'grab'
						},
						'onclick': {
							'enable': false,
							'mode': 'push'
						},
						'resize': true
					},
					'modes': {
						'grab': {
							'distance': 400,
							'line_linked': {
								'opacity': 1
							}
						},
						'bubble': {
							'distance': 400,
							'size': 40,
							'duration': 2,
							'opacity': 8,
							'speed': 3
						},
						'repulse': {
							'distance': 200,
							'duration': 0.4
						},
						'push': {
							'particles_nb': 4
						},
						'remove': {
							'particles_nb': 2
						}
					}
				},
				'retina_detect': true
			}
		);
	}
} );