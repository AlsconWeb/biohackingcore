<?php
/**
 * Functions themes.
 *
 * @package iwpdev/storefront-child
 */

use BIOH\Main;

require_once __DIR__ . '/vendor/autoload.php';

new Main();


/**
 * Post Title
 */
function post_title_shortcode(): string {
	return get_the_title();
}

add_shortcode( 'post_title', 'post_title_shortcode' );

/**
 * Output woocommerce quantity input.
 *
 * @param array|null $data Data.
 *
 * @return void
 */
function woocommerce_quantity_input( array $data = null ) {
	global $product;

	//phpcs:disable

	if ( is_cart() ) {
		foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
			$product = $cart_item['data'];
		}
	}

	if ( ! $data ) {
		$defaults = [
			'input_value' => '1',
			'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_sku(), $product ),
			'min_value'   => apply_filters( 'woocommerce_quantity_input_min', '', $product ),
			'step'        => apply_filters( 'woocommerce_quantity_input_step', '1', $product ),
			'style'       => apply_filters( 'woocommerce_quantity_style', 'float:left;', $product ),
		];
	} else {
		$defaults = [
			'input_value' => $data['input_value'],
			'step'        => apply_filters( 'cw_woocommerce_quantity_input_step', '1', $product ),
			'max_value'   => apply_filters( 'cw_woocommerce_quantity_input_max', $product->get_sku(), $product ),
			'min_value'   => apply_filters( 'cw_woocommerce_quantity_input_min', '', $product ),
			'style'       => apply_filters( 'cw_woocommerce_quantity_style', isset( $_POST['quantity'] ) ? wc_stock_amount( filter_var( wp_unslash( $_POST['quantity'] ), FILTER_SANITIZE_NUMBER_INT ) ) : $product->get_min_purchase_quantity(), $product ),
		];
	}
	//phpcs:enable
	if ( ! empty( $defaults['min_value'] ) ) {
		$min = $defaults['min_value'];
	} else {
		$min = 1;
	}
	if ( ! empty( $defaults['max_value'] ) && $defaults['max_value'] < 15 ) {
		$max = $defaults['max_value'];
	} else {
		$max = 15;
	}
	if ( ! empty( $defaults['step'] ) ) {
		$step = $defaults['step'];
	} else {
		$step = 1;
	}

	$options = '';
	for ( $count = $min; $count <= $max; $count += $step ) {
		$selected = $count === (int) $defaults['input_value'] ? ' selected' : '';
		$options  .= '<option value="' . esc_attr( $count ) . '"' . esc_attr( $selected ) . '>' . esc_html( $count ) . '</option>';
	}

	$allowed_html = [
		'option' => [
			'value'    => true,
			'title'    => true,
			'selected' => true,
		],
	];

	echo '<div class="cw_quantity_select" style="' . esc_attr( $defaults['style'] ) . '">
        <span>Qty:</span> 
        <select name="quantity" title="' . esc_html( _n( 'Qty', 'Product Description', 'woocommerce' ) ) . '" class="cw_qty">' . wp_kses( $options, $allowed_html ) . '</select>
        <span class="expected_total_price"></span>
    </div>';

	if ( ! is_cart() ) {
		echo do_shortcode( '[adp_product_bulk_rules_table]' );
	}
}

/**
 * Mini cart element.
 *
 * @return void
 */
function storefront_cart_link() {
	if ( ! storefront_woo_cart_available() ) {
		return;
	}
	?>
	<a
			class="mini-cart"
			href="#"
			title="<?php esc_attr_e( 'View your shopping cart', 'storefront-child' ); ?>">
		<?php /* translators: %d: number of items in cart */ ?>
		<?php echo wp_kses_post( WC()->cart->get_cart_subtotal() ); ?>
		<span class="count">
			<?php
			echo wp_kses_data(
				sprintf(
					_n( '%d', '%d', WC()->cart->get_cart_contents_count(), 'storefront-child' ),
					WC()->cart->get_cart_contents_count()
				)
			);
			?>
		</span>
	</a>
	<?php
}

/**
 * Copyright and link to Privacy Policy
 *
 * @return void
 */
function storefront_credit() {
	$links_output = '';

	if ( apply_filters( 'storefront_privacy_policy_link', true ) && function_exists( 'the_privacy_policy_link' ) ) {
		$separator    = '<span role="separator" aria-hidden="true"></span>';
		$links_output = get_the_privacy_policy_link( '', ( ! empty( $links_output ) ? $separator : '' ) ) . $links_output;
	}
	?>
	<div class="site-info">
		<?php echo esc_html( apply_filters( 'storefront_copyright_text', $content = '&copy; ' . get_bloginfo( 'name' ) . ' ' . gmdate( 'Y' ) ) ); ?>

		<?php if ( ! empty( $links_output ) ) { ?>
			<br/>
			<?php echo wp_kses_post( $links_output ); ?>
		<?php } ?>
	</div><!-- .site-info -->
	<?php
}
