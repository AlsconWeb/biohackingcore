<?php
defined( 'ABSPATH' ) or exit;

/**
 * @var string $header_html
 * @var array  $table_header
 * @var array  $rows
 * @var string $footer_html
 */
?>
<div class='clear'></div>

<div class="bulk_table">
	<div class="wdp_pricing_table_caption">
		<?php echo wp_kses_post( $header_html ); ?>
	</div>
	<table class="wdp_pricing_table">
		<thead>
		<tr>
			<?php foreach ( $table_header as $label ) { ?>
				<td><?php echo esc_html( $label ); ?></td>
			<?php } ?>
		</tr>
		</thead>

		<tbody>
		<?php
		foreach ( $rows as $__key => $row ) {
			$qty     = $row['qty'];
			$min_max = explode( ' - ', $qty );
			?>
			<tr
					class="wdp_pricing_table_tr"
					data-key="<?php echo esc_attr( $__key ); ?>"
					data-min="<?php echo (int) $min_max[0]; ?>"
					data-max="<?php echo isset( $min_max[1] ) ? (int) $min_max[1] : 0; ?>">
				<?php foreach ( $row as $html ) { ?>
					<td><?php echo wp_kses_post( $html ); ?></td>
				<?php } ?>
			</tr>
		<?php } ?>
		</tbody>
	</table>
</div>

<script>
	( function( $ ) {
		$( document ).ready( function() {
			$( '.cw_qty' ).change( function( e ) {
				var qty = $( this ).val();
				$( '.wdp_pricing_table_tr' ).each( function( _id ) {
					var min = $( this ).data( 'min' );
					var max = $( this ).data( 'max' );
					if ( ( qty >= min && qty <= max ) || ( qty >= min && max == 0 ) ) {
						$( this ).addClass( 'active' );
					} else {
						$( this ).removeClass( 'active' );
					}
				} );
			} );
		} );
	} )( jQuery );
</script>
