<?php
/**
 * Woocommerce Init.
 *
 * @package iwpdev/storefront-child
 */

namespace BIOH;

use WC_Order;
use WC_Product;
use WP_Term;
use wpdb;

/**
 * WoocommerceInit class file.
 */
class WoocommerceInit {

	public const BIOH_FILTER_CATEGORY_NONCE = 'product_category_filter';

	/**
	 * WoocommerceInit construct.
	 */
	public function __construct() {
		$this->init();
	}

	/**
	 * Init action.
	 *
	 * @return void
	 */
	public function init(): void {

		add_action( 'init', [ $this, 'remove_actions' ] );
		add_action( 'woocommerce_before_subcategory_title', [ $this, 'subcategory_thumbnail_customize' ], 10 );
		add_action( 'wcc_before_shop_loop_card_img', [ $this, 'shop_loop_card_img_open_tag' ], 5 );
		add_action( 'wcc_before_shop_loop_card_img', [ $this, 'shop_loop_card_img_content' ], 10 );
		add_action( 'wcc_before_shop_loop_card_img', [ $this, 'shop_loop_card_img_close_tag' ], 15 );
		add_action( 'wcc_before_shop_loop_card_content', [ $this, 'shop_loop_card_content_open_tag' ], 5 );
		add_action( 'wcc_before_shop_loop_card_content', [ $this, 'shop_loop_card_content_category' ], 10 );
		add_action( 'wcc_before_shop_loop_card_content', [ $this, 'shop_loop_card_content_title' ], 15 );
		add_action( 'wcc_before_shop_loop_card_content', [ $this, 'shop_loop_card_content_footer' ], 20 );
		add_action( 'wcc_before_shop_loop_card_content', [ $this, 'shop_loop_card_content_close_tag' ], 25 );
		add_action(
			'woocommerce_product_options_general_product_data',
			[
				$this,
				'woocommerce_product_custom_fields',
			]
		);
		add_action( 'woocommerce_process_product_meta', [ $this, 'save_woocommerce_product_custom_fields' ] );
		add_action( 'woocommerce_before_single_product', [ $this, 'move_simple_price' ] );
		add_action( 'woocommerce_single_product_entry_right', 'woocommerce_template_single_price', 5 );
		add_action( 'woocommerce_single_product_entry_right', 'woocommerce_template_single_add_to_cart', 15 );
		add_action( 'woocommerce_after_add_to_cart_button', [ $this, 'add_content_after_add_to_cart' ] );
		add_action( 'woocommerce_single_product_summary', [ $this, 'customizing_variable_products' ], 1 );
		add_action( 'woocommerce_single_product_entry_right', [ $this, 'deliver_time_single_product' ], 10 );
		add_action(
			'woocommerce_single_product_entry_right',
			[
				$this,
				'bioh_woocommerce_template_single_short_desc',
			],
			20
		);

		// filters.
		add_filter(
			'woocommerce_get_image_size_thumbnail',
			function ( $size ) {
				return [
					'width'  => 230,
					'height' => 350,
					'crop'   => 1,
				];
			}
		);
		add_filter( 'woocommerce_short_description', [ $this, 'certificate_of_analysis_link' ] );
		add_filter( 'woocommerce_checkout_fields', [ $this, 'override_checkout_fields' ] );
		add_filter( 'woocommerce_order_formatted_billing_address', [ $this, 'billing_address_fields' ], 10, 2 );
		add_filter( 'woocommerce_order_formatted_shipping_address', [ $this, 'shipping_address_fields' ], 10, 2 );
		add_filter( 'woocommerce_formatted_address_replacements', [ $this, 'add_new_replacement_fields' ], 10, 2 );
		add_filter( 'woocommerce_localisation_address_formats', [ $this, 'new_address_formats' ] );
		add_filter( 'woocommerce_cart_item_name', [ $this, 'add_cart_info' ], 99, 2 );
		add_filter( 'formatted_woocommerce_price', [ $this, 'woo_decimal_price' ], 10, 5 );
		add_filter( 'woocommerce_get_availability', [ $this, 'custom_get_availability' ], 1, 2 );
	}

	/**
	 * Remove actions.
	 *
	 * @return void
	 */
	public function remove_actions(): void {
		remove_action( 'storefront_before_content', 'woocommerce_breadcrumb', 10 );
		remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
		remove_action( 'woocommerce_before_subcategory_title', 'woocommerce_subcategory_thumbnail', 10 );
	}

	/**
	 * Subcategory thumbnail customize.
	 *
	 * @param WP_Term $category Category term.
	 *
	 * @return void
	 */
	public function subcategory_thumbnail_customize( WP_Term $category ): void {
		$thumbnail_id = get_term_meta( $category->term_id, 'thumbnail_id', true );

		if ( $thumbnail_id ) {
			$image        = wp_get_attachment_image_src( $thumbnail_id, 'full' );
			$image        = $image[0];
			$image_srcset = function_exists( 'wp_get_attachment_image_srcset' ) && wp_get_attachment_image_srcset( $thumbnail_id, 'full' );
			$image_sizes  = function_exists( 'wp_get_attachment_image_sizes' ) && wp_get_attachment_image_sizes( $thumbnail_id, 'full' );
		} else {
			$image        = wc_placeholder_img_src();
			$image_srcset = false;
			$image_sizes  = false;
		}

		if ( $image ) {
			$image = str_replace( ' ', '%20', $image );

			// Add responsive image markup if available.
			if ( $image_srcset && $image_sizes ) {
				echo '<img src="' . esc_url( $image ) . '" alt="' . esc_attr( $category->name ) . '" width="350" height="700" sizes="' . esc_attr( $image_sizes ) . '" />';
			} else {
				echo '<img src="' . esc_url( $image ) . '" alt="' . esc_attr( $category->name ) . '" width="500" height="700" />';
			}
		}
	}

	/**
	 * Shop loop card img open tag.
	 *
	 * @return void
	 */
	public function shop_loop_card_img_open_tag(): void {
		echo '<div class="card-image">';
	}

	/**
	 * Shop loop card img content.
	 *
	 * @return void
	 */
	public function shop_loop_card_img_content(): void {
		global $product;
		$prod_id = $product->get_id();

		echo '<a href="' . esc_url( get_permalink( $prod_id ) ) . '" title="' . esc_html( get_the_title( $prod_id ) ) . '">' . wp_kses_post( woocommerce_get_product_thumbnail() ) . '</a>';
		echo '<div class="ripple-container"></div>';
	}

	/**
	 * Shop loop card img close tag.
	 *
	 * @return void
	 */
	public function shop_loop_card_img_close_tag(): void {
		echo '</div>';
	}

	/**
	 * Shop loop card content open tag.
	 *
	 * @return void
	 */
	public function shop_loop_card_content_open_tag(): void {
		echo '<div class="content">';
	}

	/**
	 * Shop loop card content category.
	 *
	 * @return void
	 */
	public function shop_loop_card_content_category(): void {
		global $product;
		echo sprintf(
			'<h6 class="category">%s</h6>',
			wp_kses_post( wc_get_product_category_list( $product->get_id(), ', ', ' ', ' ' ) )
		);
	}

	/**
	 * Shop loop card content title.
	 *
	 * @return void
	 */
	public function shop_loop_card_content_title(): void {
		global $product;

		$prod_id = $product->get_id();
		echo sprintf(
			'<h4 class="card-title"><a href="%s">%s</a></h4>',
			esc_url( get_permalink( $prod_id ) ),
			esc_html( get_the_title( $prod_id ) )
		);
	}

	/**
	 * Shop loop card content footer.
	 *
	 * @param array|null $args Arguments.
	 *
	 * @return void
	 */
	public function shop_loop_card_content_footer( $args = [] ): void {
		global $product;

		$price              = '';
		$stats              = '';
		$add_to_cart_button = '';
		if ( $product ) {
			$defaults = [
				'quantity'   => 1,
				'class'      => implode(
					' ',
					array_filter(
						[
							'button',
							'product_type_' . $product->get_type(),
							$product->is_purchasable() && $product->is_in_stock() ? 'add_to_cart_button' : '',
							$product->supports( 'ajax_add_to_cart' ) && $product->is_purchasable() && $product->is_in_stock() ? 'ajax_add_to_cart' : '',
						]
					)
				),
				'attributes' => [
					'data-product_id'  => $product->get_id(),
					'data-product_sku' => $product->get_sku(),
					'aria-label'       => $product->add_to_cart_description(),
					'rel'              => 'nofollow',
				],
			];

			$args = apply_filters( 'woocommerce_loop_add_to_cart_args', wp_parse_args( $args, $defaults ), $product );

			if ( isset( $args['attributes']['aria-label'] ) ) {
				$args['attributes']['aria-label'] = wp_strip_all_tags( $args['attributes']['aria-label'] );
			}

			$quantity  = esc_attr( $args['quantity'] ?? 1 );
			$ajax_url  = esc_url( $product->add_to_cart_url() );
			$btn_class = esc_attr( $args['class'] ?? 'button' );
			$btn_attr  = isset( $args['attributes'] ) ? wc_implode_html_attributes( $args['attributes'] ) : '';
			if ( ! $product->managing_stock() && ! $product->is_in_stock() ) {
				$add_to_cart_button = sprintf(
					'<a href="%s" data-quantity="%s" class="%s" %s  style="pointer-events: none; opacity: 0.4"></a>',
					esc_url( $ajax_url ),
					esc_attr( $quantity ),
					esc_attr( $btn_class ),
					$btn_attr
				);
			} else {
				$add_to_cart_button = sprintf(
					'<a href="%s" data-quantity="%s" class="%s" %s rel="nofollow"></a>',
					esc_url( $ajax_url ),
					esc_attr( $quantity ),
					esc_attr( $btn_class ),
					$btn_attr
				);
			}
		}
		$price .= '<div class="price"><h4>' . wp_kses_post( wc_price( $product->get_price() ) ) . '</h4></div>';
		$stats .= '<div class="stats">' . wp_kses_post( $add_to_cart_button ) . '</div>';
		echo '<div class="footer">' . wp_kses_post( $price . $stats ) . '</div>';
	}

	/**
	 * Shop loop card content close tag.
	 *
	 * @return void
	 */
	public function shop_loop_card_content_close_tag(): void {
		echo '</div>';
	}

	/**
	 * Add product custom fields.
	 *
	 * @return void
	 */
	public function woocommerce_product_custom_fields(): void {
		$args = [
			'id'    => 'certificate_of_analysis_link',
			'label' => __( 'Certificate of Analysis Link', 'coal' ),
		];
		woocommerce_wp_text_input( $args );
	}

	/**
	 * Save woocommerce product custom fields.
	 *
	 * @param int|string $post_id Product ID.
	 *
	 * @return void
	 */
	public function save_woocommerce_product_custom_fields( $post_id ): void {
		$product = wc_get_product( $post_id );
		// phpcs:disable
		$custom_fields_certificate_link = isset( $_POST['certificate_of_analysis_link'] ) ? filter_var( wp_unslash( $_POST['certificate_of_analysis_link'] ), FILTER_SANITIZE_STRING ) : '';
		// phpcs:enable
		$product->update_meta_data( 'certificate_of_analysis_link', sanitize_text_field( $custom_fields_certificate_link ) );
		$product->save();
	}

	/**
	 * Filter certificate of analysis link.
	 *
	 * @param string $description Short description.
	 *
	 * @return string
	 */
	public function certificate_of_analysis_link( string $description ) {
		$product_id = get_the_ID();
		$link       = get_post_meta( $product_id, 'certificate_of_analysis_link', true );

		if ( $link ) {
			return $description . '<div style="margin-top: 40px;"><a class="hover-underline-animation" href="' . esc_url( $link ) . '" target="_blank" style="color: #6d6d6d;padding-bottom: 15px">' . __( 'Certificate of Analysis', 'storefront-child' ) . '</a></div>';
		}

		return $description;
	}

	/**
	 * Move simple price.
	 *
	 * @return void
	 */
	public function move_simple_price(): void {
		global $product;
		if ( $product->is_type( 'simple' ) ) {
			remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
			remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
		}
	}

	/**
	 * Add to woocommerce template single excerpt.
	 *
	 * @return void
	 */
	public function bioh_woocommerce_template_single_short_desc(): void {
		?>
		<ul class="right-entry-short-desc">
			<li>
				<img src="<?php echo esc_url( get_stylesheet_directory_uri() . '/images/1Asset-4tw.png' ); ?>">
				<?php esc_attr_e( 'Your Biohacking Experts', 'storefront-child' ); ?>
			</li>
			<li>
				<img src="<?php echo esc_url( get_stylesheet_directory_uri() . '/images/1Asset-4tw.png' ); ?>">
				<?php esc_attr_e( 'Mo-Fr Same day Shipping For Orders Before 17:00', 'storefront-child' ); ?>
			</li>
			<li>
				<img src="<?php echo esc_url( get_stylesheet_directory_uri() . '/images/1Asset-4tw.png' ); ?>">
				<?php esc_attr_e( 'Free Shipping For Orders Above 75eu', 'storefront-child' ); ?>
			</li>
			<li>
				<img src="<?php echo esc_url( get_stylesheet_directory_uri() . '/images/1Asset-4tw.png' ); ?>">
				<?php esc_attr_e( 'Superior Quality High Purity Extracts and Compunds', 'storefront-child' ); ?>
			</li>
			<li>
				<img src="<?php echo esc_url( get_stylesheet_directory_uri() . '/images/1Asset-4tw.png' ); ?>">
				<?php esc_attr_e( 'Quantity Based Discounts Applicable', 'storefront-child' ); ?>
			</li>
		</ul>
		<style>
			ul.right-entry-short-desc {
				list-style: none;
				font-size: 11px;
				margin: 0;
				text-align: left
			}

			.right-entry-short-desc li {
				margin-bottom: 10px;
			}

			.right-entry-short-desc li img {
				width: 14px;
				float: left;
				margin-right: 15px;
			}
		</style>
		<?php
	}

	/**
	 * Add content after add to cart.
	 *
	 * @return void
	 */
	public function add_content_after_add_to_cart(): void {
		$current_product_id = get_the_ID();
		$product            = wc_get_product( $current_product_id );
		$checkout_url       = wc_get_checkout_url();

		if ( $product->is_type( 'simple' ) ) {
			?>
			<script>
				jQuery( function( $ ) {
					$( '.single_buy_now_button' ).on( 'click', function() {
						$( this ).attr( 'href', function() {
							return this.href + '&quantity=' + $( 'select.cw_qty' ).val();
						} );
					} );
				} );
			</script>
			<?php
			echo '<a href="' . esc_url( $checkout_url ) . '?add-to-cart=' . esc_attr( $current_product_id ) . '" class="single_buy_now_button button alt">' . esc_html( __( 'Buy Now', 'storefront-child' ) ) . '</a>';
			$orders_ids = $this->get_orders_ids_by_product_id( $current_product_id );
			if ( ! empty( $orders_ids ) ) {
				foreach ( $orders_ids as $order_id ) {
					$order                    = wc_get_order( $order_id );
					$order_items              = $order->get_items();
					$product_order_quantities = [];
					foreach ( $order_items as $item_id => $item ) {
						$product_name  = $item['name'];
						$item_quantity = wc_get_order_item_meta( $item_id, '_qty', true );
						if ( $product_name === $product->get_title() ) {
							$product_order_quantities[] = $item_quantity;
						}
					}
				}
			}
		}
	}

	/**
	 * Get orders ids by product id.
	 *
	 * @param int $product_id Product ID.
	 *
	 * @return array
	 */
	private function get_orders_ids_by_product_id( $product_id ): array {

		global $wpdb;
		$order_status = [ 'wc-completed', 'wc-processing', 'wc-on-hold' ];

		$status = $this->prepare_in( $order_status );
		$sql    = "SELECT order_items.order_id
					FROM {$wpdb->prefix}woocommerce_order_items as order_items
					LEFT JOIN {$wpdb->prefix}woocommerce_order_itemmeta as order_item_meta ON order_items.order_item_id = order_item_meta.order_item_id
					LEFT JOIN {$wpdb->posts} AS posts ON order_items.order_id = posts.ID
					WHERE posts.post_type = 'shop_order'
					AND posts.post_status IN ( '%s' )
					AND order_items.order_item_type = 'line_item'
					AND order_item_meta.meta_key = '_product_id'
					AND order_item_meta.meta_value = '%s'
					ORDER BY order_items.order_id DESC";

		//phpcs:disable
		$results = $wpdb->get_col(
			$wpdb->prepare(
				$sql,
				$status,
				$product_id
			)
		);

		//phpcs:enable

		return $results;
	}

	/**
	 * Changes array of items into string of items, separated by comma and sql-escaped.
	 *
	 * @see https://coderwall.com/p/zepnaw
	 * @global wpdb       $wpdb
	 *
	 * @param mixed|array $items  item(s) to be joined into string.
	 * @param string      $format %s or %d.
	 *
	 * @return string Items separated by comma and sql-escaped
	 */
	private function prepare_in( $items, $format = '%s' ) {
		global $wpdb;

		$items    = (array) $items;
		$how_many = count( $items );

		if ( $how_many > 0 ) {
			$placeholders    = array_fill( 0, $how_many, $format );
			$prepared_format = implode( ',', $placeholders );
			// phpcs:ignore WordPress.DB.PreparedSQL.NotPrepared
			$prepared_in = $wpdb->prepare( $prepared_format, $items );
		} else {
			$prepared_in = '';
		}

		return $prepared_in;
	}

	/**
	 * Override checkout fields.
	 *
	 * @param array $fields Checkout Fields.
	 *
	 * @return array
	 */
	public function override_checkout_fields( array $fields ): array {
		$fields['billing']['billing_address_2']['placeholder'] = __( 'Street Number', 'storefront-child' );
		$fields['billing']['billing_address_2']['priority']    = 70;
		$fields['billing']['billing_address_2']['label']       = __( 'Street Number', 'storefront-child' );
		$fields['billing']['billing_address_2']['required']    = true;

		$fields['billing']['billing_address_1']['placeholder'] = __( 'Street Name', 'storefront-child' );
		$fields['billing']['billing_address_1']['label']       = __( 'Street Name', 'storefront-child' );

		$fields['billing']['billing_houseno'] = [
			'label'       => 'Addition',
			'placeholder' => 'Addition',
			'priority'    => 61,
			'required'    => false,
			'clear'       => true,
		];

		$fields['shipping']['shipping_houseno'] = [
			'label'       => 'Addition',
			'placeholder' => 'Addition',
			'priority'    => 61,
			'required'    => false,
			'clear'       => true,
		];

		return $fields;
	}

	/**
	 * Add billing house no billing_address_fields
	 *
	 * @param array    $fields Filed.
	 * @param WC_Order $order  Order.
	 *
	 * @return mixed
	 */
	public function billing_address_fields( array $fields, WC_Order $order ) {
		$fields['billing_houseno'] = get_post_meta( $order->get_id(), '_billing_houseno', true );

		return $fields;
	}

	/**
	 * Add Shipping House  to Address Fields
	 *
	 * @param array    $fields Fields.
	 * @param WC_Order $order  Order.
	 *
	 * @return array
	 */
	public function shipping_address_fields( array $fields, WC_Order $order ): array {
		$fields['shipping_houseno'] = get_post_meta( $order->get_id(), '_shipping_houseno', true );

		return $fields;
	}

	/**
	 * Create 'replacements' for new Address Fields.
	 *
	 * @param array $replacements Replacements.
	 * @param array $address      Address.
	 *
	 * @return array
	 */
	public function add_new_replacement_fields( array $replacements, array $address ): array {
		$replacements['{billing_houseno}']  = $address['billing_houseno'] ?? '';
		$replacements['{shipping_houseno}'] = $address['shipping_houseno'] ?? '';

		return $replacements;
	}

	/**
	 * Show Shipping & Billing House # for different countries.
	 *
	 * @param array $formats Formats.
	 *
	 * @return array
	 */
	public function new_address_formats( array $formats ): array {
		$formats['default'] = "{name}\n{company}\n{address_1}, {address_2}-{billing_houseno}\n{shipping_houseno}\n{state}\n{postcode} {city}\n{country}";
		$formats['IE']      = "{name}\n{company}\n{address_1}, {address_2}-{billing_houseno}\n{shipping_houseno}\n{state}\n{postcode} {city}\n{country}";
		$formats['UK']      = "{name}\n{company}\n{address_1}, {address_2}-{billing_houseno}\n{shipping_houseno}\n{state}\n{postcode} {city}\n{country}";
		$formats['NL']      = "{name}\n{company}\n{address_1}, {address_2}-{billing_houseno}\n{shipping_houseno}\n{state}\n{postcode} {city}\n{country}";
		$formats['US']      = "{name}\n{company}\n{address_1}, {address_2}-{billing_houseno}\n{shipping_houseno}\n{state}\n{postcode} {city}\n{country}";

		return $formats;
	}

	/**
	 * Add to  cart info.
	 *
	 * @param string $name      Name.
	 * @param array  $cart_item Cart item.
	 *
	 * @return mixed|string
	 */
	public function add_cart_info( $name, $cart_item ) {
		$product_item = $cart_item['data'];

		if ( ! empty( $product_item ) && $product_item->is_type( 'variation' ) ) {
			$product_cat = get_the_terms( $cart_item['product_id'], 'product_cat' );
			$add_sku     = $product_item->get_sku();
			$description = version_compare( WC_VERSION, '3.0', '<' ) ? $product_item->get_variation_description() : $product_item->get_description();
			$add_price   = $product_item->get_price_html();
			$category    = '<div class="product-cat">' . esc_html( $product_cat[0]->name ) . '</div>';
			$sku         = '<div class="product-sku">#' . esc_html( $add_sku ) . '</div>';
			$var_desc    = '<div class="var-desc">' . wp_kses_post( $description ) . '</div>';
			$price       = '<div class="price-new">' . wp_kses_post( $add_price ) . '</div>';

			return $name . '<br>' . $category . '<strong>:&nbsp;</strong>' . $sku . $var_desc . $price;
		}

		return $name;
	}

	/**
	 * Customizing variable products.
	 *
	 * @return void
	 */
	public function customizing_variable_products(): void {
		global $product;

		if ( ! $product->is_type( 'variable' ) ) {
			return;
		}

		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
		add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 15 );
	}


	/**
	 * Change woo decimal price.
	 *
	 * @param float  $formatted_price    Formatted price.
	 * @param float  $price              Price.
	 * @param int    $decimal_places     Decimal places.
	 * @param string $decimal_separator  Decimal separator.
	 * @param string $thousand_separator Thousand separator.
	 *
	 * @return string
	 */
	public function woo_decimal_price( $formatted_price, $price, int $decimal_places, string $decimal_separator, string $thousand_separator ): string {
		$unit    = number_format( $price, 0, $decimal_separator, $thousand_separator );
		$decimal = sprintf( '%02d', ( $price - (int) $price ) * 100 );

		return $unit . '<sup>' . $decimal . '</sup>';
	}

	/**
	 * Custom get availability.
	 *
	 * @param array      $availability Availability.
	 * @param WC_Product $_product     Product.
	 *
	 * @return mixed
	 */
	public function custom_get_availability( array $availability, WC_Product $_product ) {
		if ( $_product->is_in_stock() ) {
			$availability['availability'] = __( 'In Stock.', 'storefront-child' );
		}
		if ( ! $_product->is_in_stock() ) {
			$availability['availability'] = __( 'Out of Stock.', 'storefront-child' );
		}

		return $availability;
	}

	/**
	 * Deliver time in Single Product Page
	 *
	 * @return void
	 * @todo rewrite this shite.
	 */
	public function deliver_time_single_product(): void {

		$request_model   = new GetCountryRequest();
		$ip              = $request_model->get_ip_address();
		$country_details = $request_model->get_location( $ip );
		$country_name    = $country_details['country'];

		$today                 = gmdate( 'D' );
		$nextday               = gmdate( 'D', strtotime( 'next day' ) );
		$current_hour          = (int) gmdate( 'H' );
		$next_date             = gmdate( 'M-d', strtotime( 'tomorrow' ) );
		$next_date_time        = gmdate( 'Y/m/d 17:00:00', strtotime( 'tomorrow' ) );
		$next_monday_date      = gmdate( 'M-d', strtotime( 'next monday' ) );
		$next_monday_date_time = gmdate( 'Y/m/d 17:00:00', strtotime( 'next monday' ) );

		$delivery_str = '<p style="font-size: 13px; margin: 5px 0;">
        <img src="' . esc_url( get_stylesheet_directory_uri() . '/images/location_icon.png' ) . '" width=11 style="margin-right: 5px; float: left" alt="location_icon" />
        <span style="margin-right: 30px; float: left">Deliver to <span class="delivery-time-country-name">' . esc_html( $country_name ) . '</span></span>
        <span class="delivery-time-indicater" style="cursor: pointer;">
            <img src="' . esc_url( get_stylesheet_directory_uri() . '/images/del_time_icon.png' ) . '" width=15 style="margin-right: -2px; display: inline-block;" alt="del_time_icon"/>
            <span class="arrow-down" style="border-top-color: #28566d;font-size: 6px; margin-left: 5px; "><span>
        </span>
    </p>
    <p class="delivery-info-txt active"></p>';
		if ( 'Sat' !== $today && 'Sun' !== $today ) {
			if ( $current_hour < 17 ) {
				echo '
                <p style="font-size: 13px; margin: 10px 0 0 0; line-height: 18px">' . esc_html( __( 'For shipment today, order within', 'storefront-child' ) ) . ' <strong><span id="hms_timer"></span></strong>. </p > ' . wp_kses_post( $delivery_str ) . '<script>
				       jQuery( function () {
					       jQuery( "#hms_timer" ) . countdowntimer({
                        dateAndTime : "' . esc_attr( gmdate( 'Y/m/d' ) ) . ' 17:00:00",
                        size : "lg"
                   		 });
                } );</script>';
			} else {

				if ( 'Sat' === $today || 'Sun' === $today ) {

					echo '<p style="font-size: 13px; margin: 10px 0 0 0; line-height: 18px" >' . esc_html( __( 'For shipment on the', 'storefront-child' ) ) . esc_html( $next_monday_date ) . ' ' . esc_html( __( 'order within', 'storefront-child' ) ) . ' <strong><span id="hms_timer" ></span ></strong >.</p> ' . wp_kses_post( $delivery_str ) . '<script>
                           jQuery( function () {
	                           jQuery( "#hms_timer" ) . countdowntimer({
                        		dateAndTime : "' . esc_attr( $next_monday_date_time ) . '",
                        		size : "lg"
                    		});
                } );</script>';

				} else {
					if ( 'Sat' === $nextday ) {
						echo '<p style="font-size: 13px; margin: 10px 0 0 0; line-height: 18px">' . ' ' . esc_html( __( 'For shipment on the', 'storefront-child' ) ) . esc_html( $next_monday_date ) . ' ' . esc_html( __( 'order within', 'storefront-child' ) ) . '<strong><span id="hms_timer" ></span></strong>.</p>' . wp_kses_post( $delivery_str ) . '<script>
                               jQuery( function () {
	                               jQuery( "#hms_timer" ) . countdowntimer({
										dateAndTime : "' . esc_attr( $next_monday_date_time ) . '",
										size : "lg"
                       				 });
                    			} );
                    		</script>';
					} else {
						echo '<p style="font-size: 13px; margin: 10px 0 0 0; line-height: 18px" >' . esc_html( __( 'For shipment on the', 'storefront-child' ) ) . ' ' . esc_html( $next_date ) . ' ' . esc_html( __( 'order within', 'storefront-child' ) ) . '<strong><span id="hms_timer" ></span ></strong >.</p> ' . wp_kses_post( $delivery_str ) . '<script>
                               jQuery( function () {
	                               jQuery( "#hms_timer" ) . countdowntimer({
										dateAndTime : "' . esc_attr( $next_date_time ) . '",
										size : "lg"
                       				 });
                   				 } );
                   		 </script> ';
					}
				}
			}
		} else {
			echo '<p style="font-size: 13px; margin: 10px 0 0 0; line-height: 18px" >' . esc_html( __( 'For shipment on the', 'storefront-child' ) ) . ' ' . esc_html( $next_monday_date ) . ' ' . esc_html( __( 'order within', 'storefront-child' ) ) . '<strong><span id="hms_timer" ></span ></strong >.</p >' . wp_kses_post( $delivery_str ) . '<script>
                   jQuery( function () {
	                   jQuery( "#hms_timer" ) . countdowntimer({
							dateAndTime : "' . esc_attr( $next_monday_date_time ) . '",
							size : "lg"
           				 });
       				 } );
        		</script> ';
		}
	}
}
