<?php
/**
 * Add Short Codes TOC to theme.
 *
 * @package iwpdev/storefront-child
 */

namespace BIOH\ShortCodes;

/**
 * TOC class file.
 */
class TOC {
	/**
	 * ShortCodes construct.
	 */
	public function __construct() {
		$this->init();
	}

	/**
	 * Init actions.
	 *
	 * @return void
	 */
	public function init(): void {
		add_shortcode( 'TOC', [ $this, 'toc_shortcode' ] );
		add_filter( 'the_content', [ $this, 'auto_id_headings' ] );
	}

	/**
	 * Output Short code TOC.
	 *
	 * @return false|string
	 */
	public function toc_shortcode() {
		return $this->get_toc( $this->auto_id_headings( get_the_content() ) );
	}

	/**
	 * Generate short code body.
	 *
	 * @param string $content Content.
	 *
	 * @return false|string
	 */
	private function get_toc( string $content ) {

		$headings = $this->get_headings( $content );

		ob_start();
		echo '<div class="table-of-contents">';
		echo '<span class="toc-headline">' . esc_html( __( 'Table Of Contents', 'storefront-child' ) ) . '</span>';
		echo '<!-- Table of contents by webdeasy.de -->';
		echo '<span class="toggle-toc custom-setting" title="collapse">−</span>';
		$this->parse_toc( $headings, 0, 0 );
		echo '</div>';

		return ob_get_clean();
	}

	/**
	 * Get headings.
	 *
	 * @param string $content Content.
	 *
	 * @return array
	 */
	private function get_headings( string $content ): array {
		$headings = [];
		preg_match_all( '/<h([1-6])(.*)>(.*)<\/h[1-6]>/', $content, $matches );

		$counter = count( $matches[1] );

		for ( $i = 0; $i < $counter; $i ++ ) {

			$headings[ $i ]['tag'] = $matches[1][ $i ];

			$att_string = $matches[2][ $i ];
			preg_match( '/id=\"([^\"]*)\"/', $att_string, $id_matches );
			$headings[ $i ]['id'] = $id_matches[1];

			preg_match_all( '/class=\"([^\"]*)\"/', $att_string, $class_matches );

			foreach ( $class_matches[1] as $value ) {
				$headings[ $i ]['classes'][] = $value;
			}

			$headings[ $i ]['name'] = $matches[3][ $i ];
		}

		return $headings;
	}

	/**
	 * Parse toc.
	 *
	 * @param array $headings          Headings.
	 * @param int   $index             Index.
	 * @param int   $recursive_counter Recursive counter.
	 *
	 * @return void
	 */
	private function parse_toc( array $headings, int $index, int $recursive_counter ): void {
		// prevent errors.
		if ( $recursive_counter > 60 || ! count( $headings ) ) {
			return;
		}

		// get all needed elements.
		$last_element    = $index > 0 ? $headings[ $index - 1 ] : null;
		$current_element = $headings[ $index ];
		$next_element    = $index < count( $headings ) ? $headings[ $index + 1 ] : null;

		// end recursive calls.
		if ( null === $current_element ) {
			return;
		}

		// get all needed variables.
		$tag  = (int) $headings[ $index ]['tag'];
		$id   = $headings[ $index ]['id'];
		$name = $headings[ $index ]['name'];

		// element not in toc.
		if ( $current_element['classes'] && in_array( 'nitoc', $current_element['classes'], true ) ) {
			$this->parse_toc( $headings, $index + 1, $recursive_counter + 1 );

			return;
		}

		// parse toc begin or toc subpart begin.
		if ( null === $last_element || $last_element['tag'] < $tag ) {
			echo '<ul>';
		}

		// build li class.
		$li_classes = '';
		if ( $current_element['classes'] && in_array( 'toc-bold', $current_element['classes'], true ) ) {
			$li_classes = ' class="bold"';
		}

		// parse line begin.
		echo '<li' . esc_attr( $li_classes ) . '>';

		// only parse name, when li is not bold.
		if ( $current_element['classes'] && in_array( 'toc-bold', $current_element['classes'], true ) ) {
			echo esc_html( $name );
		} else {
			echo '<a href="#' . esc_attr( $id ) . '">' . esc_html( $name ) . '</a>';
		}

		if ( (int) $next_element['tag'] > $tag ) {
			$this->parse_toc( $headings, $index + 1, $recursive_counter + 1 );
		}

		// parse line end.
		echo '</li>';

		// parse next line.
		if ( (int) $next_element['tag'] === $tag ) {
			$this->parse_toc( $headings, $index + 1, $recursive_counter + 1 );
		}

		// parse toc end or toc subpart end.
		if ( null === $next_element || (int) $next_element['tag'] < $tag ) {
			echo '</ul>';
		}

		// parse top subpart.
		if ( null !== $next_element && (int) $next_element['tag'] < $tag ) {
			$this->parse_toc( $headings, $index + 1, $recursive_counter + 1 );
		}
	}

	/**
	 * Add auto id headings.
	 *
	 * @param string $content Content.
	 *
	 * @return array|string|string[]|null
	 */
	public function auto_id_headings( string $content ) {
		$content = preg_replace_callback(
			'/(\<h[1-6](.*?))\>(.*)(<\/h[1-6]>)/i',
			'self::static_preg_replace_callback',
			$content
		);

		return $content;
	}

	/**
	 * Test to static function
	 *
	 * @param array $matches Matches.
	 *
	 * @return mixed|string
	 * @todo need check.
	 */
	private function static_preg_replace_callback( array $matches ) {
		if ( ! stripos( $matches[0], 'id=' ) ) {
			$matches[0] = $matches[1] . $matches[2] . ' id="' . sanitize_title( $matches[3] ) . '">' . $matches[3] . $matches[4];
		}

		return $matches[0];
	}
}
