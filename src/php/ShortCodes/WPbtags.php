<?php
/**
 * Add Short Codes wpbtags to theme.
 *
 * @package iwpdev/storefront-child
 */

namespace BIOH\ShortCodes;

/**
 * WPbtags class file.
 */
class WPbtags {
	/**
	 * WPbtags construct.
	 */
	public function __construct() {
		$this->init();
	}

	/**
	 * Init actions.
	 *
	 * @return void
	 */
	public function init(): void {
		add_shortcode( 'wpbtags', [ $this, 'wpb_tags' ] );
	}

	/**
	 * Output tag cloud.
	 *
	 * @return string
	 */
	public function wpb_tags(): string {

		$wpbtags = get_tags();
		$string  = '';

		foreach ( $wpbtags as $tag ) {
			$string .= '<span class="tagbox">' . esc_html( __( 'Tags:', 'storefront-child' ) ) . ' <a class="taglink" href="' . esc_url( get_tag_link( $tag->term_id ) ) . '">' . esc_html( $tag->name ) . '</a>' . "\n";
		}

		return $string;
	}
}
