<?php
/**
 * Custom code to CT7.
 *
 * @package iwpdev/storefront-child
 */

namespace BIOH;

/**
 * CF7 class file.
 */
class CF7 {

	/**
	 * Country array data.
	 *
	 * @var array|mixed
	 */
	private $country_data;

	public $country_code;

	/**
	 * CF7 construct.
	 */
	public function __construct() {
		$this->init();
	}

	/**
	 * Init actions.
	 *
	 * @return void
	 */
	public function init(): void {
		add_action( 'wp_ajax_cf7_populate_values', [ $this, 'ajax_cf7_populate_values' ] );
		add_action( 'wp_ajax_nopriv_cf7_populate_values', [ $this, 'ajax_cf7_populate_values' ] );
		add_action( 'init', [ $this, 'set_country_day' ] );
		add_action( 'wp_footer', [ $this, 'set_country_code_from_js' ] );
	}

	/**
	 * Ajax handler populate values.
	 *
	 * @return void
	 */
	public function ajax_cf7_populate_values(): void {

		$data = $this->get_country_array();

		$return_array = [
			'countries'       => $data,
			'current_country' => false,
			'delivery_time'   => false,
		];

		$country = array_key_exists( 'country', $_POST ) ? $_POST['country'] : false;

		if ( $country ) {
			$return_array['current_country'] = $country;
			$return_array['delivery_time']   = $data[ $country ];
		}

		echo json_encode( $return_array );
		wp_die();
	}

	/**
	 * Get county array.
	 *
	 * @return array
	 */
	public function get_country_array(): array {
		$countries = [];

		if ( empty( $this->country_data ) ) {
			return [];
		}

		foreach ( $this->country_data as $datum ) {
			$countries[ $datum['bioh_country_name'] ] = [
				'code' => $datum['bioh_country_code'],
				'min'  => $datum['bioh_country_delivery_min'],
				'max'  => $datum['bioh_country_delivery_max'],
			];
		}

		return $countries;
	}

	/**
	 * Set country day.
	 *
	 * @return void
	 */
	public function set_country_day(): void {
		$this->country_data = carbon_get_theme_option( 'bioh_countrys' );
		$this->country_code = $this->get_county_code();
	}

	/**
	 * Get country code.
	 *
	 * @return string[]
	 */
	public function get_county_code(): array {

		if ( empty( $this->country_data ) ) {
			return [
				'be',
				'bg',
				'cz',
				'de',
				'fi',
				'fr',
				'gr',
				'hu',
				'ie',
				'it',
				'lv',
				'lt',
				'no',
				'nz',
				'pl',
				'ro',
				'sk',
				'se',
			];
		}

		$country_code = [];

		foreach ( $this->country_data as $datum ) {
			$country_code[] = strtolower( $datum['bioh_country_code'] );
		}

		return $country_code;
	}

	/**
	 * Add script localization country code.
	 *
	 * @return void
	 */
	public function set_country_code_from_js(): void {
		$country_list = $this->get_county_code();
		?>
		<script type="application/javascript">
			var countryList = <?php echo wp_json_encode( $country_list ); ?>;
		</script>
		<?php
	}
}
