<?php
/**
 * Job filter.
 *
 * @package iwpdev/storefront-child
 */

namespace BIOH;

use WP_Query;

/**
 * JobFilter class file.
 */
class ProductFilter {

	/**
	 * Select work diary array.
	 *
	 * @var array
	 */
	public array $work_diary;

	/**
	 * Nonce cod name.
	 */
	public const BIOH_FILTER_NONCE_NAME = 'bioh_filter_submit';

	/**
	 * Code Match.
	 *
	 * @var string[] Invalid characters.
	 */
	private array $code_match = [
		'"',
		'!',
		'@',
		'#',
		'$',
		'%',
		'^',
		'&',
		'*',
		'(',
		')',
		'_',
		'+',
		'{',
		'}',
		'|',
		':',
		'"',
		'<',
		'>',
		'?',
		'[',
		']',
		';',
		"'",
		',',
		'.',
		'/',
		'',
		'~',
		'`',
		'=',
	];


	/**
	 * JobFilter construct.
	 */
	public function __construct() {
		$this->work_diary = [
			'full'     => __( 'Полный рабочий день', 'pr' ),
			'shift'    => __( 'Cменный график', 'pr' ),
			'flexible' => __( 'Гибкий график', 'pr' ),
			'remote'   => __( 'Удаленная работа', 'pr' ),
		];

		$this->url_rewrite_rules();

		add_action( 'pre_get_posts', [ $this, 'add_query_filter' ] );
		add_filter( 'query_vars', [ $this, 'add_filter_vars' ] );

		add_action( 'admin_post_nopriv_bioh_filter', [ $this, 'prod_filter_handler' ] );
		add_action( 'admin_post_bioh_filter', [ $this, 'prod_filter_handler' ] );
	}


	/**
	 * Update Main query product from filter.
	 *
	 * @param WP_Query $query Main Query.
	 *
	 * @return WP_Query
	 */
	public function add_query_filter( WP_Query $query ): WP_Query {

		if (
			! is_admin() && $query->is_main_query() &&
			is_post_type_archive( 'product' ) &&
			! empty( $query->query_vars['filter'] )
		) {
			$args          = [];
			$filter_params = $this->parse_url_query( $query->query_vars['filter'] );
			foreach ( $filter_params as $key => $param ) {
				switch ( $key ) {
					case 'type':
						$args['tax_query'][] =
							[
								'taxonomy' => 'product_cat',
								'field'    => 'slug',
								'terms'    => $param,
							];
						break;
					case 'effect':
						$args['tax_query'][] = [
							'taxonomy' => 'product_tag',
							'field'    => 'slug',
							'terms'    => $param,
						];
						break;
				}
			}

			foreach ( $args as $key => $arg ) {
				$query->set( $key, $arg );
			}

			return $query;
		}

		return $query;
	}


	/**
	 * Show filter.
	 *
	 * @return void
	 */
	public function show_filter(): void {
		$cat_args = [
			'orderby'    => 'name',
			'order'      => 'ASC',
			'hide_empty' => false,
		];

		$product_categories = get_terms( 'product_cat', $cat_args );
		$product_tags       = get_terms( 'product_tag', $cat_args );
		$filter             = $this->parse_url_query( get_query_var( 'filter' ) ) ?? [];
		$active_tab         = '';
		$active_filter      = '';

		if ( ! empty( $filter ) ) {
			$active_tab    = array_keys( $filter )[0];
			$active_filter = array_slice( $filter[ $active_tab ], 0, 1 )[0];
		}
		?>
		<form
				class="form-category"
				method="post"
				id="bioh_filter"
				action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>">
			<div class="tab">
				<a
						class="tablinks <?php echo 'type' === $active_tab || empty( $active_tab ) ? 'active' : ''; ?>"
						data-tab="1">
					<?php esc_html_e( 'Type', 'storefront-child' ); ?>
				</a>
				<a class="tablinks <?php echo 'effect' === $active_tab ? 'active' : ''; ?>" data-tab="2">
					<?php esc_html_e( 'Effect', 'storefront-child' ); ?>
				</a>
			</div>
			<div
					class="tabcontent <?php echo 'type' === $active_tab || empty( $active_tab ) ? 'open' : ''; ?>"
					data-tab="1">
				<?php if ( ! empty( $product_categories ) ) { ?>
					<div class="category-filter">
						<?php foreach ( $product_categories as $category ) { ?>
							<label
									class="btn-filter <?php echo $category->slug === $active_filter ? 'active' : ''; ?>"
									for="btn-filter-<?php echo esc_html( $category->slug ); ?>">
								<?php echo esc_html( $category->name ); ?>
							</label>
							<input
									class="hidden-btn-filter"
									id="btn-filter-<?php echo esc_html( $category->slug ); ?>"
									type="submit"
									name="bioh_filter[type][]"
									value="<?php echo esc_html( $category->slug ); ?>">
						<?php } ?>
					</div>
				<?php } ?>
			</div>
			<div class="tabcontent <?php echo 'effect' === $active_tab ? 'open' : ''; ?>" data-tab="2">
				<?php if ( ! empty( $product_tags ) ) { ?>
					<div class="tag-filter">
						<?php foreach ( $product_tags as $tag ) { ?>
							<label
									class="btn-filter <?php echo $tag->slug === $active_filter ? 'active' : ''; ?>"
									for="btn-filter-<?php echo esc_html( $tag->slug ); ?>">
								<?php echo esc_html( $tag->name ); ?>
							</label>
							<input
									class="hidden-btn-filter"
									id="btn-filter-<?php echo esc_html( $tag->slug ); ?>"
									type="submit"
									name="bioh_filter[effect][]"
									value="<?php echo esc_html( $tag->slug ); ?>">
						<?php } ?>
					</div>
				<?php } ?>
			</div>
			<input type="hidden" name="action" value="bioh_filter">
			<?php wp_nonce_field( self::BIOH_FILTER_NONCE_NAME, self::BIOH_FILTER_NONCE_NAME ); ?>
		</form>
		<?php
	}

	/**
	 * Parse string URL
	 *
	 * @param string $url_query URL Query.
	 *
	 * @return array|false
	 */
	public function parse_url_query( string $url_query ) {
		$param_string = $url_query;
		$query_arg    = [];
		if ( ! empty( $param_string ) ) {
			$params = explode( '-and-', $param_string );
			foreach ( $params as $param ) {
				$items = explode( '-in-', urldecode( $param ) );
				if ( empty( $items[1] ) ) {
					continue;
				}
				$param_name = $items[0];
				unset( $items[0] );
				if ( preg_match( '-or-', $items[1] ) ) {
					$query_arg[ $param_name ] = explode( '-or-', $items[1] );
				} else {
					$query_arg[ $param_name ] = $items;
				}
			}

			return array_filter( $query_arg );
		}

		return false;
	}

	/**
	 * Generate Url Redirect
	 */
	public function prod_filter_handler(): void {
		if ( ! isset( $_POST[ self::BIOH_FILTER_NONCE_NAME ] ) && ! wp_verify_nonce( self::BIOH_FILTER_NONCE_NAME, self::BIOH_FILTER_NONCE_NAME ) ) {
			wp_safe_redirect( filter_input( INPUT_POST, '_wp_http_referer', FILTER_SANITIZE_STRING ) );
		}

		$request = ! empty( $_REQUEST['bioh_filter'] ) ? wp_unslash( $_REQUEST['bioh_filter'] ) : [];
		$request = array_filter( $request );

		$params = [];
		foreach ( $request as $key => $item ) {
			$item = array_filter( (array) $item );
			if ( ! empty( $item ) ) {
				$params[ $key ] = [];

				foreach ( $item as $value ) {
					$params[ $key ][] = rawurlencode( str_replace( $this->code_match, '-', $value ) );
				}
			}
		}

		$redirect = get_bloginfo( 'url' ) . '/shop/filter/';
		$i        = 0;
		foreach ( $params as $key => $param ) {
			if ( 0 === $i ) {
				$redirect .= $key . '-in-' . implode( '-or-', (array) $param );
				$i ++;
				continue;
			}

			$param = array_filter( $param );

			// Why it is different to the above?
			foreach ( $param as $index => $item ) {
				$addon = ( 0 === $index ) ? '-and-' . $key . '-in-' : '-or-';

				$redirect .= $addon . $item;
			}
		}
		wp_safe_redirect( $redirect . '/' );
		die();
	}

	/**
	 * Add Rewrite rules url
	 */
	public function url_rewrite_rules(): void {

		add_rewrite_rule(
			'shop/filter/([-_a-zA-Z0-9+%.:]+)/page/(\d+)/?$',
			'index.php?post_type=product&filter=$matches[1]&paged=$matches[2]',
			'top'
		);

		add_rewrite_rule(
			'shop/filter/([-_a-zA-Z0-9+%.:]+)/?$',
			'index.php?post_type=product&filter=$matches[1]',
			'top'
		);
	}

	/**
	 * Add Query vars filter
	 *
	 * @param array $vars Vars Query.
	 *
	 * @return array
	 */
	public function add_filter_vars( array $vars ): array {
		$vars[] = 'filter';

		return $vars;
	}
}
