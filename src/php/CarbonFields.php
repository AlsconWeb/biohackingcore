<?php
/**
 * Add Support Carbone Fields in theme.
 *
 * @package iwpdev/storefront-child
 */

namespace BIOH;

use Carbon_Fields\Carbon_Fields;
use Carbon_Fields\Container;
use Carbon_Fields\Field;

/**
 * CarbonFields class file.
 */
class CarbonFields {

	/**
	 * CarbonFields construct.
	 */
	public function __construct() {
		$this->init();
	}

	/**
	 * Init Hooks.
	 *
	 * @return void
	 */
	public function init(): void {
		add_action( 'after_setup_theme', [ $this, 'carbon_fields_load' ] );
		add_action( 'carbon_fields_register_fields', [ $this, 'add_country_options' ] );
	}

	/**
	 * Carbon fields loader.
	 *
	 * @return void
	 */
	public function carbon_fields_load(): void {
		Carbon_Fields::boot();
	}

	/**
	 * Add country options.
	 *
	 * @return void
	 */
	public function add_country_options(): void {
		Container::make(
			'theme_options',
			__( 'Country options', 'storefront-child' )
		)->add_fields(
			[
				Field::make( 'complex', 'bioh_countrys' )->add_fields(
					[
						Field::make(
							'text',
							'bioh_country_name',
							__( 'Country name', 'storefront-child' )
						)->set_width( 50 ),

						Field::make(
							'text',
							'bioh_country_code',
							__( 'Country code', 'storefront-child' )
						)->set_width( 50 ),

						Field::make(
							'text',
							'bioh_country_delivery_min',
							__( 'Country delivery min', 'storefront-child' )
						)->set_width( 50 ),

						Field::make(
							'text',
							'bioh_country_delivery_max',
							__( 'Country delivery max', 'storefront-child' )
						)->set_width( 50 ),
					]
				),
			]
		);
	}
}
