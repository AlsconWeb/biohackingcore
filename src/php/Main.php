<?php
/**
 * Main class init theme.
 *
 * @package iwpdev/storefront-child
 */

namespace BIOH;

use BIOH\ShortCodes\TOC;
use BIOH\ShortCodes\WPbtags;

/**
 * Main class file.
 */
class Main {

	/**
	 * Theme version.
	 */
	public const BIOH_VERSION_THEME = '1.4.0';

	/**
	 * Stylesheet URL.
	 *
	 * @var string
	 */
	private string $file_url;

	/**
	 * Main construct.
	 */
	public function __construct() {
		$this->int();

		$this->file_url = get_stylesheet_directory_uri();

		new HeaderActions();
		new WoocommerceInit();
		new CF7();
		new TOC();
		new WPbtags();
		new ProductFilter();
		new CarbonFields();

//		$redirect = new GetCountryRequest();
//		$redirect->redirect_to_domain();

	}

	/**
	 * Init function.
	 *
	 * @return void
	 */
	public function int(): void {

		add_action( 'wp_enqueue_scripts', [ $this, 'add_script' ] );
		add_action( 'after_setup_theme', [ $this, 'theme_support' ] );
		add_action( 'storefront_site_branding', [ $this, 'change_custom_logo_size' ], 30 );
		add_filter( 'body_class', [ $this, 'custom_body_class' ] );
	}

	/**
	 * Add Scripts and style.
	 *
	 * @return void
	 */
	public function add_script(): void {
		// style.
		wp_enqueue_style( 'country-select-style', $this->file_url . '/lib/country-select/css/countrySelect.min.css', '', self::BIOH_VERSION_THEME );
		wp_enqueue_style( 'shadowllax-style', $this->file_url . '/lib/shadowllax/shadowllax.css', '', self::BIOH_VERSION_THEME );
		wp_enqueue_style( 'jquery-ui-style', '//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css', '', '1.12.1' );

		// script.
		wp_enqueue_script( 'jquery-ui', '//code.jquery.com/ui/1.12.1/jquery-ui.js', [ 'jquery' ], '1.12.1', true );
		wp_enqueue_script( 'jquery-countdown', $this->file_url . '/lib/countdown/countdownTimer.js', [ 'jquery' ], self::BIOH_VERSION_THEME, true );
		wp_enqueue_script( 'particles-script', $this->file_url . '/js/particles.js', [ 'jquery' ], '2.0.0', true );
		wp_enqueue_script(
			'm-script',
			$this->file_url . '/js/main.js',
			[
				'jquery',
				'particles-script',
			],
			self::BIOH_VERSION_THEME,
			true
		);
		wp_enqueue_script( 'custom-particle-bg-script', $this->file_url . '/js/custom-particle-bg.js', [ 'jquery' ], self::BIOH_VERSION_THEME, true );
		wp_enqueue_script( 'hcSticky.js', $this->file_url . '/js/hcSticky.js', [ 'jquery' ], self::BIOH_VERSION_THEME, true );
		wp_enqueue_script( 'country-select-script', $this->file_url . '/lib/country-select/js/countrySelect.js', [ 'jquery' ], self::BIOH_VERSION_THEME, true );
		wp_enqueue_script( 'shadowllax-script', $this->file_url . '/lib/shadowllax/shadowllax.js', [ 'jquery' ], self::BIOH_VERSION_THEME, true );
		wp_enqueue_script( 'stickyheader', $this->file_url . '/js/stickyheader.js', [ 'jquery' ], self::BIOH_VERSION_THEME, true );
		wp_enqueue_script(
			'country-select',
			$this->file_url . '/js/country-select.js',
			[
				'jquery',
				'country-select-script',
			],
			self::BIOH_VERSION_THEME,
			true
		);

		$class_request_model = new GetCountryRequest();
		$ip                  = $class_request_model->get_ip_address();
		$country_details     = $class_request_model->get_location( $ip );
		$country_code        = $country_details['country_code'] ?? null;
		$country_name        = $country_details['country'] ?? null;

		wp_localize_script(
			'country-select',
			'bioH',
			[
				'ajax'          => admin_url( 'admin-ajax.php' ),
				'isPageProduct' => is_single() || is_product(),
				'countryCode'   => $country_code,
				'countryName'   => $country_name,
				'test_request'  => $country_details,
			]
		);
	}

	/**
	 * Theme support.
	 *
	 * @return void
	 */
	public function theme_support(): void {
		add_theme_support( 'custom-header' );
		register_nav_menus(
			[
				'social-icon-menu' => __( 'Social Icon Menu' ),
			]
		);
	}

	/**
	 * Change custom logo size.
	 *
	 * @return void
	 */
	public function change_custom_logo_size(): void {
		add_theme_support(
			'custom-logo',
			[
				'height'     => 343,
				'width'      => 804,
				'flex-width' => true,
			]
		);
	}

	/**
	 * Add custom body class.
	 *
	 * @param array $classes Classes array.
	 *
	 * @return array
	 */
	public function custom_body_class( array $classes ): array {
		if ( is_shop() ) {
			$classes[] = 'page-template-template-fullwidth-php';
		}

		return $classes;
	}
}
