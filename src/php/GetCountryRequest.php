<?php
/**
 * Get country request.
 *
 * @package iwpdev/storefront-child.
 */

namespace BIOH;
/**
 * GetCountryRequest class file.
 */
class GetCountryRequest {
	/**
	 * GetCountryRequest construct.
	 */
	public function __construct() {

	}

	/**
	 * Get user IP.
	 *
	 * @return mixed|string
	 */
	public function get_ip_address() {

		if ( BIOH_DEBUG_IP ) {
			return '46.204.45.42';
		}

		return ! empty( $_SERVER['REMOTE_ADDR'] ) ? filter_var( wp_unslash( $_SERVER['REMOTE_ADDR'] ), FILTER_VALIDATE_IP ) : '';
	}

	/**
	 * Validation IP.
	 *
	 * @param string $ip IP Address.
	 *
	 * @return bool
	 */
	public function is_valid_ip_address( string $ip ): bool {
		return filter_var( $ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 | FILTER_FLAG_IPV6 | FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE ) !== false;
	}

	/**
	 * Get location.
	 *
	 * @param string $ip IP Address.
	 *
	 * @return mixed|null
	 */
	public function get_location( string $ip ) {

		$response = wp_remote_get( 'https://ipwhois.app/json/' . $ip );

		if ( is_wp_error( $response ) ) {
			wp_send_json_error( [ 'message' => $response->get_error_message() ] );
		}

		if ( wp_remote_retrieve_response_code( $response ) === 200 ) {
			$json = wp_remote_retrieve_body( $response );

			return json_decode( $json, true );
		}

		return (object) [];
	}

	/**
	 * Redirect to other domain
	 *
	 * @return void
	 */
	public function redirect_to_domain(): void {
		$ip              = $this->get_ip_address();
		$response        = $this->get_location( $ip );
		$redirect_status = $_COOKIE['redirect_status'] ?? false;

		if ( ! empty( $response ) && ! is_user_logged_in() ) {
			switch ( $response['country_code'] ) {
				case 'BE':
					if ( ! $redirect_status && ! is_admin() ) {
						wp_safe_redirect( 'https://biohackingcore.com/be/', 301 );
						setcookie( 'redirect_status', true, time() + 3600, COOKIEPATH, COOKIE_DOMAIN );
						exit();
					}
					break;
				case 'NL':
					if ( ! $redirect_status && ! is_admin() ) {
						wp_safe_redirect( 'https://biohackingcore.nl/', 301 );
						setcookie( 'redirect_status', true, time() + 3600, COOKIEPATH, COOKIE_DOMAIN );
						exit();
					}
					break;
				default:
					if ( ! $redirect_status && ! is_admin() ) {
						wp_safe_redirect( 'https://biohackingcore.com/', 301 );
						setcookie( 'redirect_status', true, time() + 3600, COOKIEPATH, COOKIE_DOMAIN );
						exit();
					}
			}
		}
	}
}
