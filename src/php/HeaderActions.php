<?php
/**
 * Header Actions class.
 *
 * @package iwpdev/storefront-child
 */

namespace BIOH;

use stdClass;

/**
 * HeaderActions class file.
 */
class HeaderActions {
	/**
	 * HeaderActions construct.
	 */
	public function __construct() {
		$this->init();
	}

	/**
	 * Init function.
	 *
	 * @return void
	 */
	public function init(): void {

		// add actions.
		add_action( 'top_header', [ $this, 'top_header_sub_list' ], 50 );
		add_action( 'wp_footer', [ $this, 'dynamic_dropdown_script_footer' ], 20 );
		add_action( 'storefront_header', [ $this, 'delivery_form' ], 38 );
		add_action( 'init', [ $this, 'refactor_header' ] );

		add_filter( 'wp_nav_menu_items', [ $this, 'add_logout_link' ], 10, 2 );

		add_action( 'wp_ajax_get_discounted_price_product', [ $this, 'ajax_get_discounted_price_product' ] );
		add_action( 'wp_ajax_nopriv_get_discounted_price_product', [ $this, 'ajax_get_discounted_price_product' ] );

	}

	/**
	 * Change header wrappers.
	 *
	 * @return void
	 */
	public function refactor_header(): void {
		// remove actions.
		remove_action( 'storefront_header', 'storefront_header_container', 0 );
		remove_action( 'storefront_header', 'storefront_header_container_close', 41 );
		remove_action( 'storefront_header', 'storefront_primary_navigation_wrapper', 42 );
		remove_action( 'storefront_header', 'storefront_primary_navigation_wrapper_close', 68 );

		// add actions.
		add_action( 'storefront_header', [ $this, 'header_container_div' ], 0 );
		add_action( 'storefront_header', [ $this, 'header_container_close_div' ], 41 );
		add_action( 'storefront_header', [ $this, 'primary_navigation_wrapper_div' ], 42 );
		add_action( 'storefront_header', [ $this, 'primary_navigation_wrapper_div_close' ], 68 );
		add_action( 'storefront_header', [ $this, 'social_icons' ], 35 );
	}

	/**
	 * Show top header sub line.
	 *
	 * @return void
	 */
	public function top_header_sub_list(): void {
		$items = [
			__( 'Top Quality Guaranteed', 'storefront-child' ),
			__( 'Third Party Tested', 'storefront-child' ),
			__( 'Research Backed Extracts and Compounds', 'storefront-child' ),
			__( 'Worldwide Priority Shipping', 'storefront-child' ),
			__( 'Free Tracking Included', 'storefront-child' ),
		];
		?>
		<div class="thats-why-items-container">
			<ul class="thats-why-items">
				<?php foreach ( $items as $item ) { ?>
					<li><?php echo esc_html( $item ); ?></li>
				<?php } ?>
			</ul>
		</div>
		<?php
	}

	/**
	 * Output scripts in footer.
	 *
	 * @return void
	 */
	public function dynamic_dropdown_script_footer(): void {
		if ( is_shop() || is_product() ) {
			global $product;

			if ( ! empty( $product ) ) {
				$product_id = $product->get_id();
			}
			?>
			<script>
				( function( $ ) {
					$( document ).ready( function() {
						let filter_val = getUrlVars();
						if ( filter_val.length === 0 ) {
							console.log( 'No filter applied' );
						} else {
							let keys = Object.keys( filter_val );

							if ( jQuery.inArray( 'type', keys ) !== '-1' ) {
								$( 'button[data-slug=\'' + filter_val[ 'type' ] + '\']' ).click();
							}

							if ( jQuery.inArray( 'effects', keys ) !== '-1' ) {
								$( 'button.tablinks[data-tab=2]' ).click();
								$( 'button[data-slug=\'' + filter_val[ 'effects' ] + '\']' ).click();
							}

						}

						$( 'form.cart' ).find( '.single_add_to_cart_button' ).click( function() {
							setTimeout( () => {
								$( '.cw_qty' ).val( 1 );
								$( '.wdp_pricing_table_tr' ).removeClass( 'active' );
							}, 3000 );
						} );

						$( '.cw_qty' ).change( function() {
							$( this ).parents( 'form.cart' ).find( '.single_add_to_cart_button' ).attr( 'data-quantity', $( this ).val() );
							$( '.expected_total_price' ).html(
								'<img src="<?php echo esc_url( get_stylesheet_directory_uri() . '/images/loading.gif' ); ?>" width=15 style="display: inline-block">'
							);
							get_discounted_price_product( $( this ).val() );
						} );

						$( '.delivery-time-indicater' ).click( function() {
							$( '.delivery-info-txt' ).toggleClass( 'active' );
						} );

						function getUrlVars() {
							let vars = [],
								hash;
							let hashes = window.location.href.slice( window.location.href.indexOf( '?' ) + 1 ).split( '&' );
							for ( let i = 0; i < hashes.length; i++ ) {
								hash = hashes[ i ].split( '=' );
								vars.push( hash[ 0 ] );
								vars[ hash[ 0 ] ] = hash[ 1 ];
							}
							return vars;
						}
						<?php $nonce = wp_create_nonce( 'get_discounted_price_product' ); ?>

						function get_discounted_price_product( qty ) {
							let data = {
								'action': 'get_discounted_price_product',
								'product_id': <?php echo esc_attr( $product_id ?? 0 ); ?>,
								'qty': qty,
								'nonce': "<?php echo esc_attr( $nonce ); ?>",
							};
							$.post( '<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>', data, function( response ) {
								let price = response.data.price;
								let total_price = response.data.total_price;
								let regular_total_price = response.data.regular_price;

								if ( price && total_price ) {
									if ( regular_total_price === 0 ) {
										$( '.expected_total_price' ).html(
											'<ins>' + total_price + '</ins>'
										);
									} else {
										$( '.expected_total_price' ).html(
											'<del>' + regular_total_price + '</del> ' + '<ins>' + total_price + '</ins>'
										);
									}
								} else {
									$( '.expected_total_price' ).html( '' );
								}
							}, 'json' );
						}
					} );
				} )( jQuery );
			</script>
			<?php
		}
	}

	/**
	 * Header container div.
	 *
	 * @return void
	 */
	public function header_container_div(): void {
		echo '<div class="container"><div class="row"><div class="col-12">';
	}

	/**
	 * Header container close div.
	 *
	 * @return void
	 */
	public function header_container_close_div(): void {
		echo '</div></div></div>';
	}

	/**
	 * Primary navigation wrapper div.
	 *
	 * @return void
	 */
	public function primary_navigation_wrapper_div(): void {
		echo '<div class="container"><div class="row"><div class="col-12"><div class="storefront-primary-navigation">';
	}

	/**
	 * Primary navigation wrapper div close.
	 *
	 * @return void
	 */
	public function primary_navigation_wrapper_div_close(): void {
		echo '<div></div></div></div>';
	}

	/**
	 * Delivery form.
	 *
	 * @return void
	 */
	public function delivery_form(): void {
		$items = [
			'delivery' => [
				'icon'   => get_stylesheet_directory_uri() . '/images/icon-truck.svg',
				'icon_2' => get_stylesheet_directory_uri() . '/images/icon-truck-white.svg',
				'text'   => __( 'Delivery to', 'storefront-child' ),
			],
		];
		?>
		<div class="delivery-time-container">
			<div class="delivery-time">
				<div class="delivery-time-text"><?php esc_attr_e( 'Choose your country', 'storefront-child' ); ?></div>
				<?php foreach ( $items as $key => $item ) { ?>
					<div class="item <?php echo esc_attr( $key ); ?>">
						<div>
							<span class="no-wrap"><?php echo esc_html( $item['text'] ); ?></span>
						</div>
					</div>
				<?php } ?>
				<span class="arrow-down"></span>
			</div>
			<div class="delivery-time-dropdown">
				<div class="delivery-time-dropdown-content">
					<div class="delivery-time-dropdown-form">
						<div class="item">
							<input type="text" class="ctr-select" id="ctr-select" readonly/>
						</div>
					</div>
					<div class="delivery-time-dropdown-result">
						<div class="delivery-result-text">
						</div>
						<div class="delivery-error">
							<b>
								<?php esc_attr_e( 'We don`t ship to your address!', 'storefront-child' ); ?>
							</b>
							<br/>
							<span><?php esc_attr_e( 'Due to your country law and regulations, we are not permitted to send to your location. If you have any questions please contact us', 'storefront-child' ); ?></span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
	}

	/**
	 * Social icons.
	 *
	 * @return void
	 */
	public function social_icons(): void {
		if ( has_nav_menu( 'social-icon-menu' ) ) {
			?>
			<nav
					class="social-icon-menu-navigation"
					role="navigation"
					aria-label="<?php esc_html_e( 'Social Icon Navigation', 'storefront-child' ); ?>">
				<?php
				wp_nav_menu(
					[
						'theme_location' => 'social-icon-menu',
						'fallback_cb'    => '',
					]
				);
				?>
			</nav>
			<?php
		}
	}

	/**
	 * Add login / logout link to menu .
	 *
	 * @param string   $items Item menu.
	 * @param stdClass $args  Arguments.
	 *
	 * @return mixed|string
	 */
	public function add_logout_link( string $items, stdClass $args ) {

		if ( 'secondary' === $args->theme_location && is_user_logged_in() ) {
			$items .= '<li><a href="' . wp_logout_url( get_permalink( wc_get_page_id( 'myaccount' ) ) ) . '">' . __( 'Log Out', 'storefront-child' ) . '</a></li>';
		}

		if ( 'secondary' === $args->theme_location && ! is_user_logged_in() ) {
			$items .= '<li><a href="' . get_permalink( wc_get_page_id( 'myaccount' ) ) . '">' . __( 'Log In', 'storefront-child' ) . '</a></li>';
		}

		return $items;
	}


	/**
	 * Ajax handler get discounted price product.
	 *
	 * @return void
	 */
	public function ajax_get_discounted_price_product(): void {
		$product_id = ! empty( $_POST['product_id'] ) ? filter_var( wp_unslash( $_POST['product_id'] ), FILTER_SANITIZE_STRING ) : false;
		$nonce      = ! empty( $_POST['nonce'] ) ? filter_var( wp_unslash( $_POST['nonce'] ), FILTER_SANITIZE_STRING ) : false;
		$qty        = ! empty( $_POST['qty'] ) ? filter_var( wp_unslash( $_POST['qty'] ), FILTER_SANITIZE_NUMBER_INT ) : 0;

		if ( ! wp_verify_nonce( $nonce, 'get_discounted_price_product' ) ) {
			wp_send_json_error( [ 'message' => __( 'Bad nonce code' ) ] );
		}

		$product = wc_get_product( $product_id );
		if ( $product_id ) {
			$discounted_price = adp_functions()->getDiscountedProductPrice( $product_id, $qty );
			$regular_price    = $product->get_regular_price();
			if ( $discounted_price === $regular_price ) {
				wp_send_json_success(
					[
						'price'         => wc_price( $discounted_price ),
						'total_price'   => wc_price( $discounted_price * $qty ),
						'regular_price' => 0,
					]
				);
			} else {
				wp_send_json_success(
					[
						'price'         => wc_price( $discounted_price ),
						'total_price'   => wc_price( $discounted_price * $qty ),
						'regular_price' => wc_price( $product->get_regular_price() * $qty ),
					]
				);
			}
		}

		wp_send_json_success(
			[
				'price'         => 0,
				'total_price'   => 0,
				'regular_price' => 0,
			]
		);
	}
}
